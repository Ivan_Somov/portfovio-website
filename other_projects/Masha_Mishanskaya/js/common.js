//растяжка изображения на весь экран !! сохранить !!
$(document).ready(function() {

	$("#portfolio_grid").mixItUp();

	$(".s_portfolio li").click(function(){
		$(".s_portfolio li").removeClass("active");
		$(this).addClass("active");
	});

	$(".popup").magnificPopup({type:"image"});
	$(".popup_content").magnificPopup({type:"inline", midClick: true});

	$(".top_text h1").animated("fadeInDown","fadeOutUp");
	$(".top_text p, .section_header").animated("fadeInUp","fadeOutDown");

	$(".animation_1").animated("flipInY","flipOutY");
	$(".animation_2").animated("slideInLeft","slideOutLeft");
	$(".animation_3").animated("slideInRight","slideOutRight");

	$(".right .resume_item").animated("slideInRight","slideOutRight");
	$(".left .resume_item").animated("slideInLeft","slideOutLeft");

	function heightDetect() {
		$(".main_head").css("height", $(window).height());
	};
		heightDetect()
		$(window).resize(function() {
			heightDetect()
		});

// Рзадача айдишников разным работам в меню портфолио

	$(".portfolio_item").each(function(i) {
		$(this).find("a").attr("href", "#work_" + i);
		$(this).find(".port_descr").attr("id", "work_" + i);
	});

// Валидация обратной связи
$("input, select,textarea").not("[type=submit]").jqBootstrapValidation();

// мягкий скрол при нажатии на меню
$(".top_mnu ul a").mPageScroll2id();

});

//гифка при загрузке сайта
$(window).load(function() {
	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow"); 
});

//анимация на кнопке
$(".toggle_mnu, .menu_item").click(function() {
  $(".sandwich").toggleClass("active");
});
// скрыть меню
	$(".top_mnu ul a").click(function() {
		$(".top_mnu").fadeOut(600);
		$(".sandwich").toggleClass("active");
		$(".top_text").css("opacity", "1");
	}).append("<span>");
//анимация появления меню
$(".toggle_mnu").click(function() {
	if ($(".top_mnu").is(":visible")){
		$(".top_text").removeClass("h-opasifi");
		$(".top_mnu").fadeOut(600);
		$(".top_mnu").removClass("pulse animated");
		}else{
		$(".top_text").addClass("h-opasifi");
		$(".top_mnu").fadeIn(600);
		$(".top_mnu").addClass("pulse animated");
		};
});
/*анимация плавного движения курсора по меню
$(".top-mnu").sliphover({
	target:"a",
	backgroundColor: "rgba(255,255,255,.05)"
});*/
