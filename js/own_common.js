

	/* Loader delaying */
$(document).ready(function() {


var options = {
  offset: 500
}
var header = new Headhesive('.main_menu', options);

var navsection = $(".main_menu");
$(".tracked").waypoint (function() {
	var hash = $(this).attr("id");
	$.each(navsection, function() {
		if( $(this).children("a").attr("href").slice(1) == hash){
			$(this).children("a").children("div").addClass("active_button_main_menu");
		}
	});
});

$(document).ready(function() {

	/* Animation */
	$('.logo_name h1').addClass('animated fadeInDown');
	$('.logo_name p').addClass('animated fadeInUp');

	/* Window height detect */
	function heightDetect() {
		$(".main_head").css("height", $(window).height());
	};
		heightDetect()
		$(window).resize(function() {
			heightDetect()
		});

		/* Soft scroll */
	$(".about a").mPageScroll2id();
	$(".button_menu a").mPageScroll2id();
	$(".main_menu a").mPageScroll2id();


});

$("input, select,textarea").not("[type=submit]").jqBootstrapValidation();
});
